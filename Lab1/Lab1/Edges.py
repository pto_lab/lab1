from numpy import *
from scipy import *
from scipy.signal import *
from pylab import *
from Util import *

def RunEdgeDetectionAndSharpening(edgeFilter, sharpeningFilter, noiseStandardDeviation):

    image = readImage()   
    
    noisyImage = addNoise(image, noiseStandardDeviation)
    
    verticalEdges = sumForAllColors(noisyImage, edgeFilter.FilterVertical)
    horizontalEdges = sumForAllColors(noisyImage, edgeFilter.FilterHorizontal)
    
    allEdges = combineEdges(horizontalEdges, verticalEdges)

    sharpenedimg = runForAllColors(noisyImage, sharpeningFilter.Sharpen)
    
    print(noisyImage.shape[0:2])
    showImage(noisyImage, 'Obraz testowy')
    showImage(verticalEdges, 'Krawedzie V')
    showImage(horizontalEdges, 'Krawedzie H')
    showImage(allEdges, 'Krawedzie')       
    showImage(sharpenedimg, 'Obraz wyostrzony')

    show()

def readImage():
    filename = input('Nazwa pliku?\n')
    testimg = imread(filename)
    return testimg

def addNoise(image, standardDeviation):  
    noise = np.random.normal(0, standardDeviation, image.shape);
    imageNoiseSum = image + noise;
    return clip(imageNoiseSum, 0.0, 1.0)

def sumForAllColors(image, operation):
    if(isColor(image)):
        colorChannels = image.shape[2]
        result = zeros(image.shape[0:2])
        for i in range(0, colorChannels):
            result += operation(image[:, :, i])
    else:
        result = operation(image)
    return result    

def isColor(image):
    return len(image.shape) > 2

def combineEdges(horizontalEdges, verticalEdges):
    edgesSum = verticalEdges + horizontalEdges 
    clipped = clip(edgesSum, 0.0, 1.0)
    return clipped

def runForAllColors(image, operation):
    if(isColor(image)):
        colorChannels = image.shape[2]
        result = zeros(image.shape)
        for i in range(0, colorChannels):
            result[:, :, i] = operation(image[:, :, i])
    else:
        result = operation(image)
    return result

def showImage(img, imgTitle):
    figure()                                        
    axis('off')                                     
    title(imgTitle)                                 
    imshow(img, cmap=cm.gray, vmin=0.0, vmax=1.0)    