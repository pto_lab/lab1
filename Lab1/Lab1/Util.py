from numpy import *
from scipy import *
from scipy.signal import *
from pylab import *

def filterImage(image, mask):
    filteredImage = convolve2d(image, mask, mode='same', boundary='symm')
    return filteredImage