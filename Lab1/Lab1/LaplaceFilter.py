from SharpeningFilter import *
from numpy import *

class LaplaceFilter(SharpeningFilter):
    
    def GetMask(self):
        return array([[-1, -1, -1], [-1, 8, -1], [-1, -1, -1]], float)
