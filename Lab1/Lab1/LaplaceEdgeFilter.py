from EdgeFilter import *
from numpy import *

class LaplaceEdgeFilter(EdgeFilter):

    def GetHorizontalMask(self):
        return array([[0, -1, 0], [0, 2, 0], [0, -1, 0]], float)

    def GetVerticalMask(self):
        return array([[0, 0, 0], [-1, 2, -1], [0, 0, 0]], float)
