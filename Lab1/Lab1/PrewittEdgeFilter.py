from EdgeFilter import *
from numpy import *

class PrewittEdgeFilter(EdgeFilter):
    """Filtr Prewitta"""
    
    def GetHorizontalMask(self):
        return array([[1, 1, 1],[0, 0, 0],[-1, -1, -1]], float)
    
    def GetVerticalMask(self):
        return array([[1, 0, -1], [1, 0, -1], [1, 0, -1]], float)