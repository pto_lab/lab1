from Util import *
from abc import *

class EdgeFilter(metaclass=ABCMeta):
    
    @abstractmethod
    def GetVerticalMask(self):
        pass    
    
    @abstractmethod
    def GetHorizontalMask(self):
        pass

    def FilterVertical(self, image):
        return self.__filter(image, self.GetVerticalMask())

    def FilterHorizontal(self, image):
        return self.__filter(image, self.GetHorizontalMask())

    def __filter(self, image, mask):
        filteredImage = filterImage(image, mask)
        return abs(filteredImage)