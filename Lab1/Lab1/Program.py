from Edges import *;
from SobelEdgeFilter import *
from PrewittEdgeFilter import *
from LaplaceFilter import *
from LaplaceEdgeFilter import *

noiseStandardDeviation = float(input('Odchylenie standardowe szumu: '))

filters = {'l' : LaplaceEdgeFilter(),
           'p' : PrewittEdgeFilter(),
           's' : SobelEdgeFilter()}

symbol = input('Filtr (l - Laplace, p - Prewitt, s - Sobel): ')

filter = filters[symbol]

sharpeningHighPassWeight = float(input('Waga skladowej wysokoczestotliwosciowej (wyostrzanie filterm Laplaca): '))

sharpeningFilter = LaplaceFilter()
sharpeningFilter.SetHighPassWeight(sharpeningHighPassWeight)

RunEdgeDetectionAndSharpening(filter, sharpeningFilter, noiseStandardDeviation) 