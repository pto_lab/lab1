from abc import *
from Util import *

class SharpeningFilter(metaclass=ABCMeta):
    
    def __init__(self):
        self.highPassWeight = 0.5

    def SetHighPassWeight(self, weight):
        self.highPassWeight = weight
    
    def GetHighPassWeight(self):
        return self.highPassWeight

    def Sharpen(self, image):
        highPass = filterImage(image, self.GetMask())
        sharpenedImage = clip(image + self.highPassWeight * highPass, 0.0, 1.0)
        return sharpenedImage

    @abstractmethod
    def GetMask(self):
        pass

