from EdgeFilter import *
from numpy import *

class SobelEdgeFilter(EdgeFilter):
    
    def GetHorizontalMask(self):
        return array([[1, 2, 1], [0, 0, 0], [-1, -2, -1]], float)
    
    def GetVerticalMask(self):
        return array([[1, 0, -1], [2, 0, -2], [1, 0, -2]], float)

